﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]

// the public float feature allows for user (player) to determine the boundaries by manually implementing
// I am wondering if it is worth considering hardcoding the dimensions in here as opposed to doing it in unity

public class  Boundary
{
	public float xMin, xMax, zMin, zMax;
}

//class inherits from MonoBehaviour
public class PlayerController : MonoBehaviour 
{
	public float speed; 
	public float tilt; 
	// lowercase b is the name for reference in the code
	public Boundary boundary;
	private Rigidbody rig1;

	public GameObject shot; 
	public Transform shotSpawn; 
	public float fireRate; 

	private float nextFire; 


	// will update before every frame
	// I am wondering if there is any possibility that we could use a different method to trigger the shot as opposed to mouse clicking? 
	// Could we switch the Input.GetButton feature to use the spacebar as opposed to the mouse clicks? YES: Edit --> Project Settings --> Input --> "Fire1" --> space


	void Update ()
	{
		if (Input.GetButton("Fire1") && Time.time > nextFire)
		{
			nextFire = Time.time + fireRate;

			//referencing new GameObject (so instantiate is necessary)
			Instantiate (shot, shotSpawn.position, shotSpawn.rotation); // as GameObject;

		}
	}
		
	void Start()
	{
		rig1 = GetComponent<Rigidbody> ();
	}

	void FixedUpdate()
	{
		float moveHorizonatal = Input.GetAxis ("Horizontal"); 
		float moveVertical = Input.GetAxis ("Vertical"); 

		Vector3 movement = new Vector3 (moveHorizonatal, 0.0f, moveVertical); 
		rig1.velocity = movement * speed;
		rig1.position = new Vector3
			(
				Mathf.Clamp (rig1.position.x, boundary.xMin, boundary.xMax),
				0.0f,
				Mathf.Clamp (rig1.position.z, boundary.zMin, boundary.zMax)
			);
	
		rig1.rotation = Quaternion.Euler (0.0f, 0.0f, rig1.velocity.x * -tilt); 
	}
}
